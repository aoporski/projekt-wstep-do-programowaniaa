import random
import sys

def menu():
    while True:
        print("GRA WISIELEC\n[1] Zagraj\n[2] Wyjdź")
        try:
            choice = int(input("Wybierz opcję: "))
            if choice == 1:
                return str(input("Podaj nazwę uzytkownika: "))
            elif choice == 2:
                sys.exit()
            else:
                print("Wpisz 1 lub 2!")
                choice = int(input("Wybierz opcję: "))
        except ValueError:
            print("Musisz podać liczbę: ")
    
def poziom_trudnosci():
    while True:
        try:
            print("[1] Łatwy\n[2] Trudny")
            wybor = int(input("Wybierz poziom trudności:"))
        except ValueError:
            print("Musisz podać liczbę: ")
        latwy = 4
        trudny = 2

        if wybor == 1:
            return latwy
        elif wybor == 2:
            return trudny
        else:
            print("Podaj 1 albo 2.")


def game(poziom):

    kola_ratunkowe = poziom

    print(f"GRAJ!!!\nZGADNIJ HASŁO!!!\nAby użyć koło ratunkowe wciśnij x, mozesz ich użyc tylko {kola_ratunkowe} razy na jedno hasło!")


    with open('hasla.txt', 'r') as file:
        lines = file.readlines()
        for i in lines:
            haslo = random.choice(lines).strip()
        

        x = 0
        for _ in haslo:
            x += 1
        wyswietl = x * "_".split(" ")
        print(wyswietl)


        bledy = 0
        while "_" in wyswietl and bledy < 7:
            zgadnij = input("Podaj literę: ")

            if len(zgadnij) != 1 or not zgadnij.isalpha():
                print("Musisz podać jedną literę!")
                continue

            if kola_ratunkowe > 0:
                if zgadnij == "x":
                    kola_ratunkowe -= 1
                    index_kolo = random.choice(range(len(wyswietl)))
                    while wyswietl[index_kolo] != '_':
                        index_kolo = random.choice(range(len(wyswietl)))

                    print(f"Oto twoje koło ratunkowe. Zostało ci ich {kola_ratunkowe} ")
                    kolo = haslo[index_kolo]
                    wyswietl[index_kolo] = kolo
                    for i in range(len(wyswietl)):
                        if haslo[i] == kolo:
                            wyswietl[i] = kolo
                    print(wyswietl)
                    continue
                        

            for i in range(len(wyswietl)):
                if haslo[i] == zgadnij:
                    wyswietl[i] = zgadnij
            print(wyswietl)

            if zgadnij not in haslo:
                bledy += 1
            print(rysuj(bledy))

        if "_" not in wyswietl:
            print("Wygrałeś!!! Zgadnij następne hasło: ")
            return 1
        else:
            print(f"Przegrałeś :(\nPoprawne hasło to: {haslo}")
            return 0
        
def rysuj(bledy):
    wisielec = [
        '''
    ___________________
        ||
        ||
        ||
        ||
        ||
        ||
        ||
        ||
        ||
        ||
      TTTTTTTTTTTTTTTT


        '''
    ,
        '''
    ___________________
        ||          |
        ||
        ||
        ||
        ||
        ||
        ||
      TTTTTTTTTTTTTTTT

      '''

    ,
        '''
    ___________________
        ||          |
        ||          0
        ||      
        ||
        ||
        ||
        ||
      TTTTTTTTTTTTTTTT

      '''
    ,
    '''
    ___________________
        ||          |
        ||          0
        ||          |
        ||          |
        ||
        ||
        ||
      TTTTTTTTTTTTTTTT

      '''
    ,
        '''
    ___________________
        ||          |
        ||          0
        ||          |}
        ||          |
        ||
        ||
        ||
      TTTTTTTTTTTTTTTT

      '''
    ,
            '''
    ___________________
        ||          |
        ||          0
        ||         {|}
        ||          |
        ||           
        ||
        ||
      TTTTTTTTTTTTTTTT

      '''
    ,
                '''
    ___________________
        ||          |
        ||          0
        ||         {|}
        ||          |
        ||           }
        ||
        ||
      TTTTTTTTTTTTTTTT

      '''
    ,
                    '''
    ___________________
        ||          |
        ||          0
        ||         {|}
        ||          |
        ||         { }
        ||
        ||
      TTTTTTTTTTTTTTTT

      '''

    ]
    
    return wisielec[bledy]


        

def main():
    username = menu()
    total_points = 0
    poziom = poziom_trudnosci()
    if poziom == 4:
        trudnosc = 'łatwy'
    if poziom == 2: 
        trudnosc = 'trudny'

    while True:
        result = game(poziom)

        if result == 1:
            total_points += 1
        elif result == 0:
            with open("punktacja.txt", "a") as file:
                file.write(f"użytkownik: {username}; punkty: {total_points}, poziom trudności: {trudnosc}\n")
            break



main()
